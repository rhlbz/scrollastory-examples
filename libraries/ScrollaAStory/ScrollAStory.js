var global_controller = new ScrollMagic.Controller();
var global_controller_parallax = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

class ScrollAStory {

    constructor ()
    {
        this.controller = global_controller;
        this.controller_parallax = global_controller_parallax;
        this.total_sections;
        this.achored_sections;
        this.sections_info = [] ;
        this.show_anchor_points;
        this.type_anchor_points;
        this.fixed_backgrounds = [];
        this.steps_pinnings_elements = [];
        this.steps_discreete_pinnings_elements = [];
        this.steps_continuous_pinnings_elements = [];
        this.is_ready = false;
    }

    // TEXT_FLOW
    Add_Static_Section(id_section, is_anchored, title)
    {
        var static_section = document.getElementById(id_section);
        var title_text_flow = document.createAttribute("title");
        if ( is_anchored )
            static_section.classList.add("anchored");
        static_section.classList.add("static_section");
        title_text_flow.value = title;
        static_section.setAttributeNode(title_text_flow);
    }
    // PAGE_FLOW
    Add_Scrolly_Section(id_section, is_anchored, title)
    {
        var scrolly_section = document.getElementById(id_section);
        var title_page_flow = document.createAttribute("title");
        if ( is_anchored )
            scrolly_section.classList.add("anchored");
        scrolly_section.classList.add("scrolly_section");
        title_page_flow.value = title;
        scrolly_section.setAttributeNode(title_page_flow);
    }

    // ANIMATION_FLOW
    Add_Animation_Section(id_section, is_anchored, title)
    {
        var animation_section = document.getElementById(id_section);
        var title_animation_flow = document.createAttribute("title");
        var value_class;
        if ( is_anchored )
            animation_section.classList.add("anchored");
        animation_section.classList.add("animation_section");
        title_animation_flow.value = title;
        animation_section.setAttributeNode(title_animation_flow);
    }

    // PHOTOGRAPHY SECTION
    Add_Photography_Section(id_section, is_anchored, title)
    {
        var photography_section = document.getElementById(id_section);
        var title_photography_section = document.createAttribute("title");
        if ( is_anchored )
            photography_section.classList.add("anchored");
        photography_section.classList.add("photography_section");
        title_photography_section.value = title;
        photography_section.setAttributeNode(title_photography_section);
    }

// * * * * * * * * * * * PINNING ELEMENTS * * * * * * * * * * * //
    Insert_Pinning_Element(section_id, position, pins_width)
    {
        // id della sezione
        // posizione dell'elemento pinnabile [left, right, center]
        // dimensione dell'elemento pinnabile in numeri percentuali, NULL se la posizione è center
        var section = document.getElementById(section_id);
        var children = section.children;
        var trigger_id;
        var content;
        var pinning_element;
        var other_description;
        // var stylesheet = window.document.styleSheets[0];
        for ( var i = 0; i < children.length; i++ )
        {
            switch(children[i].className)
            {
                case 'section_trigger_point':
                    trigger_id = children[i]
                break;
                case 'section_content':
                    content = children[i]
                break;
                case 'section_pinning_element':
                    pinning_element = children[i]            
                break;
            }
        }
        // stylesheet.insertRule("@keyframes example {0%   {background-color: red;} 25%  {background-color: yellow;} 50%  {background-color: blue;}100% {background-color: green;}}", 0);
        // stylesheet.insertRule("@-webkit-keyframes example {0%   {background-color: red;} 25%  {background-color: yellow;} 50%  {background-color: blue;} 100% {background-color:  green;}}", 0);
        switch( position )
        {
            case 'left':
                {
                    var content_width = 97 - pins_width;
                    pinning_element.style.width = "" + pins_width + "%";
                    pinning_element.children[0].style.width = "100%";
                    pinning_element.children[0].style.float = "left";
                    content.style.float = "right";
                    content.style.width = "" + content_width + "%";
                    var client_height = pinning_element.children[0].clientHeight;
                    var scene_duration = content.offsetHeight - client_height;
                }
            break;
            case 'right':
                {
                    var marginLeft_pinId =  99 - pins_width;            
                    pinning_element.style.width = "" + pins_width + "%";
                    pinning_element.style.marginLeft = "" + marginLeft_pinId + "%";
                    pinning_element.style.textAlign = "right";
                    pinning_element.children[0].style.width = "100%";
                    pinning_element.children[0].style.textAlign = "right";
                    var width_content =  97 - pins_width;
                    content.style.width = "" + width_content + "%";
                    content.style.float = "left";
                    var client_height = pinning_element.children[0].clientHeight;
                    var scene_duration = content.offsetHeight - client_height;
                }
            break;
        }
        this.CreateScene(trigger_id, scene_duration, pinning_element);
    }
// * * * * * * * * * * * VARIOUS PINNINGS ELEMENTS * * * * * * * * * * * //
    Insert_Variable_Pinnings_Elements(section_id, position, pins_width)
    {
        var section = document.getElementById(section_id);
        var children = section.children;
        var trigger_id;
        var content;
        var pinning_element;

        for ( var i = 0; i < children.length; i++ )
        {
            switch(children[i].className)
            {
                case 'section_trigger_point':
                    trigger_id = children[i]
                break;
                case 'section_content':
                    content = children[i]
                break;
                case 'section_pinning_element':
                    pinning_element = children[i]            
                break;
            }
        }
        var contents_steps = content.children;
        var pinnings_element = pinning_element.children;
        var section_steps = [];
        for ( var i = 0; i < contents_steps.length; i++ )
        {
            var triggs_info = {               
                'start' : contents_steps[i].offsetTop,
                'end' : contents_steps[i].offsetTop + contents_steps[i].offsetHeight
            };
            section_steps.push({
                'trigg_id' : contents_steps[i].id,
                'pin_id' : pinnings_element[i].id,
                'triggs_info' : triggs_info
            });
            
        }
        this.steps_pinnings_elements[section_id] = {
            'section_start' : content.offsetTop,
            'section_end' : content.offsetTop + content.offsetHeight,
            'section_steps' : section_steps
        };
        var last_pinned_element = pinning_element.children.length - 1;
        switch ( position )
        {
            case 'left':
                {
                    pinning_element.style.width = "" + pins_width + "%";
                    var content_width = 97 - pins_width;
                    for ( var i = 0; i < pinning_element.children.length; i++ )
                    {
                        pinning_element.children[i].style.width = "100%";
                        pinning_element.children[i].style.float = "left";
                    }
                    content.style.width = "" + content_width + "%";
                    content.style.float = "right";                    
                }
            break;
            case 'right':
                {
                    var pin_offset_height;
                    pinning_element.style.width = "" + pins_width + "%";
                    var marginLeft_pinId =  99 - pins_width;   
                    var width_content =  97 - pins_width;
                    for ( var i = 0; i < pinning_element.children.length; i++ )
                    {
                        pinning_element.children[i].style.width = "100%";
                        pinning_element.children[i].style.float = "right";
                        pin_offset_height = pinning_element.children[i].clientHeight;
                    }
                    pinning_element.style.width = "" + pins_width + "%";
                    pinning_element.style.marginLeft = "" + marginLeft_pinId + "%";
                    pinning_element.style.textAlign = "right";
                    content.style.width = "" + width_content + "%";
                    content.style.float = "left";                }
            break;
        }
        var scene_duration = scene_duration = content.offsetHeight - pinning_element.children[last_pinned_element].clientHeight;
        this.CreateScene(trigger_id, scene_duration, pinning_element);
    }

    Insert_Discreete_Pinning_Element(section_id, position, pins_width)
    {
        var section = document.getElementById(section_id);
        
        var children = section.children;
        var trigger_id;
        var content;
        var pinning_element;
        
        for ( var i = 0; i < children.length; i++ )
        {
            switch(children[i].className)
            {
                case 'section_trigger_point':
                    trigger_id = children[i]
                break;
                case 'section_content':
                    content = children[i]
                break;
                case 'section_pinning_element':
                    pinning_element = children[i]            
                break;
            }
        }
        var contents_steps = content.children;
        var pinnings_element = pinning_element.children;
        var pinned_element = pinning_element.children[0];
        var triggs_info = {};
        for ( var i = 0; i < contents_steps.length; i++ )
        {
            if ( contents_steps[i].id  != "" )
            {
                
                console.log("contents_steps[i].offsetTop, ", contents_steps[i].offsetTop,)
                triggs_info[contents_steps[i].id] = {               
                    'start' : contents_steps[i].offsetTop,
                    'end' : contents_steps[i].offsetTop + contents_steps[i].offsetHeight,
                    'value' : contents_steps[i].attributes.value.nodeValue
                };
            }

        }

        this.steps_discreete_pinnings_elements[section_id] = {
            'section_start' : content.offsetTop,
            'section_end' : content.offsetTop + content.offsetHeight,
            'triggs' : triggs_info,
            'pinned_element' : pinned_element
        };
        var last_pinned_element = pinning_element.children.length - 1;
        switch ( position )
        {
            case 'left':
                {
                    pinning_element.style.width = "" + pins_width + "%";
                    var content_width = 97 - pins_width;
                    for ( var i = 0; i < pinning_element.children.length; i++ )
                    {
                        pinning_element.children[i].style.width = "100%";
                        pinning_element.children[i].style.float = "left";
                    }
                    content.style.width = "" + content_width + "%";
                    content.style.float = "right";                    
                }
            break;
            case 'right':
                {
                    var pin_offset_height;
                    pinning_element.style.width = "" + pins_width + "%";
                    var marginLeft_pinId =  99 - pins_width;   
                    var width_content =  97 - pins_width;
                    for ( var i = 0; i < pinning_element.children.length; i++ )
                    {
                        pinning_element.children[i].style.width = "100%";
                        pinning_element.children[i].style.float = "right";
                        pin_offset_height = pinning_element.children[i].clientHeight;
                    }
                    pinning_element.style.width = "" + pins_width + "%";
                    pinning_element.style.marginLeft = "" + marginLeft_pinId + "%";
                    pinning_element.style.textAlign = "right";
                    content.style.width = "" + width_content + "%";
                    content.style.float = "left";                }
            break;
        }
        var scene_duration = scene_duration = content.offsetHeight - pinning_element.children[last_pinned_element].clientHeight;
        this.CreateScene(trigger_id, scene_duration, pinning_element);
    }

    Insert_Continuous_Pinning_Element(section_id, position, pins_width)
    {
        var section = document.getElementById(section_id);
        
        var children = section.children;
        var trigger_id;
        var content;
        var pinning_element;
        
        for ( var i = 0; i < children.length; i++ )
        {
            switch(children[i].className)
            {
                case 'section_trigger_point':
                    trigger_id = children[i]
                break;
                case 'section_content':
                    content = children[i]
                break;
                case 'section_pinning_element':
                    pinning_element = children[i]            
                break;
            }
        }
        var contents_steps = content.children;
        var pinnings_element = pinning_element.children;
        var pinned_element = pinning_element.children[0];

        this.steps_continuous_pinnings_elements[section_id] = {
            'section_start' : content.offsetTop,
            'section_end' : content.offsetTop + content.offsetHeight,
            'section_height' :  content.offsetHeight,
            'pinned_element' : pinned_element
        };
        var last_pinned_element = pinning_element.children.length - 1;
        switch ( position )
        {
            case 'left':
                {
                    pinning_element.style.width = "" + pins_width + "%";
                    var content_width = 97 - pins_width;
                    for ( var i = 0; i < pinning_element.children.length; i++ )
                    {
                        pinning_element.children[i].style.width = "100%";
                        pinning_element.children[i].style.float = "left";
                    }
                    content.style.width = "" + content_width + "%";
                    content.style.float = "right";                    
                }
            break;
            case 'right':
                {
                    var pin_offset_height;
                    pinning_element.style.width = "" + pins_width + "%";
                    var marginLeft_pinId =  99 - pins_width;   
                    var width_content =  97 - pins_width;
                    for ( var i = 0; i < pinning_element.children.length; i++ )
                    {
                        pinning_element.children[i].style.width = "100%";
                        pinning_element.children[i].style.float = "right";
                        pin_offset_height = pinning_element.children[i].clientHeight;
                    }
                    pinning_element.style.width = "" + pins_width + "%";
                    pinning_element.style.marginLeft = "" + marginLeft_pinId + "%";
                    pinning_element.style.textAlign = "right";
                    content.style.width = "" + width_content + "%";
                    content.style.float = "left";                }
            break;
        }
        var scene_duration = scene_duration = content.offsetHeight - pinning_element.children[last_pinned_element].clientHeight;
        this.CreateScene(trigger_id, scene_duration, pinning_element);
    }


//  * * * * * * * * * * * REVEALING COMPONENT  * * * * * * * * * * * //
    Insert_Revealing_Component(section_id, position)
    {
        var section = document.getElementById(section_id);
        var children = section.children;
        var trigger_id;
        var revealing_section;
        for (var i = 0; i < children.length; i++ )
        {
            switch(children[i].className)
            {
                case 'section_trigger_point':
                    trigger_id = children[i]
                break;
                case 'section_revealing':
                    revealing_section = children[i]
                break;
            }
        }
        revealing_section.classList.add("section_revealing_pin");
        var children = revealing_section.children;
        var children_classes = {};
        for ( var c = 0; c < children.length; c++ )
        {
            var child_class = children[c].className.split(" ");
            children[c].style.textAlign = "" + position + "";
            children_classes[children[c].id] = child_class;
            var type;
            var first_parameter;
            var second_parameter;
            for ( var t = 0; t < child_class.length; t++ )
            {
                var child_type = child_class[t].split("-");
                if ( child_type.length > 1 )
                {
                    for ( var p = 0; p < child_type.length; p++ )
                    {
                        var class_parameters = child_type[p].split(".");
                        if ( class_parameters.length > 1 )
                        {
                            type = child_type[0];
                            first_parameter = class_parameters[0];
                            second_parameter = class_parameters[1]
                        }
                        else 
                        {
                            type = child_type[0];
                            first_parameter = child_type[1];
                        }
                    }
                }
                else
                {
                    type = child_type[0];
                    first_parameter = child_type[1];
                }
                switch (type)
                {
                    case 'pinning_element_section':
                        {
                            this.Insert_Pinning_Element(children[c].id, first_parameter, second_parameter);
                        }
                    break;
                    case 'image_sequence_section':
                        {
                            var images_sources = sections[i].getElementsByClassName("images_sources");
                            var sources = []
                            for ( var img = 0; img < images_sources.length; img++ )
                            {
                                sources.push(images_sources[img].src);
                            }
                            this.Insert_Pinning_Image_Sequence(children[c].id, sources, first_parameter, second_parameter);
                        }
                    break;
                    case 'text_on_fixed_backgrounds':
                        {
                            this.Insert_Pinning_Background(children[c].id, first_parameter)
                        }
                    break;
                    case 'variable_pinning_elements':
                        {
                            this.Insert_Variable_Pinnings_Elements(children[c].id, first_parameter, second_parameter);
                        }
                    break;
                }
            }
        }
        
        this.Set_Steps_Variable_Pinnings_Elements();
        this.CreateRevealingScene(trigger_id, revealing_section);
    }    
// * * * * * * * * * * * IMAGE SEQUENCES * * * * * * * * * * * //
    Insert_Pinning_Image_Sequence(section_id, sources, position, pins_width)
    {
        var section = document.getElementById(section_id);
        var children = section.children;
        var trigger_id;
        var description;
        var pinning_element;
        var other_description;
        for ( var i = 0; i < children.length; i++ )
        {
            switch(children[i].className)
            {
                case 'section_trigger_point':
                    trigger_id = children[i]
                break;
                case 'section_description':
                    description = children[i]
                break;
                case 'section_pinning_element':
                    pinning_element = children[i]            
                break;
                case 'section_other_description':
                    other_description = children[i];
            }
        }
        switch( position )
        {
            case 'left':
                {
                    pinning_element.style.width = "" + pins_width + "%";
                    pinning_element.style.float = "left";
                    var width_description =  99 - pins_width;
                    description.style.width = "" + width_description + "%";
                    description.style.float = "right";
                    var scene_duration = description.offsetHeight;
                }
            break;
            case 'right':
                {
                    pinning_element.style.width = "" + pins_width + "%";
                    pinning_element.style.float = "right";
                    var marginLeft_pinId =  99 - pins_width;
                    var width_description =  99 - pins_width;
                    pinning_element.style.marginLeft = "" + marginLeft_pinId + "%";
                    description.style.width = "" + width_description + "%";
                    description.style.float = "left";
                    var scene_duration = description.offsetHeight;
                }
            break;
        }
        var window_height = window.innerHeight;
        var repeating = scene_duration / window_height;
        repeating.toFixed(0);
        var obj = { curImg: 0 };
        var tween = TweenMax.to(obj, 0.5,
            {
                curImg: sources.length - 1,	// animate propery curImg to number of images
				roundProps: "curImg",				// only integers so it can be used as an array index
				repeat: repeating,									// repeat 3 times
				immediateRender: false,			// load first image automatically
				ease: Linear.easeNone,			// show every image the same ammount of time
					onUpdate: function () {
						$(pinning_element).attr("src", sources[obj.curImg]); // set the image source
				}
            }
        );

        if ( position == 'left' )
        {
            var scene_duration = description.offsetHeight - pinning_element.getBoundingClientRect().width;
        }
        else 
        {
            var scene_duration = description.offsetHeight - description.offsetLeft;
        }
        this.CreateImagesSequence(trigger_id, scene_duration, tween, pinning_element)
    }
// * * * * * * * * * * * PARALLAX * * * * * * * * * * * //
    Full_Screen_Image_And_Center_Text(trigger_point_id, element_id)
    {
        var trigger = document.getElementById(trigger_point_id);
        var element = document.getElementById(element_id);
        var background;
        //var element = document.getElementById(element_id);
        for(var i= 0; i < trigger.childNodes.length; i++ )
        {
            if (trigger.childNodes[i].style != undefined )
            {
                background = trigger.childNodes[i].style.backgroundImage;
            }
        }
        var element_background = document.createAttribute("style");
        element_background.value = "position: absolute; z-index: 10; text-align: center";
        element.setAttributeNode(element_background);
        var tween = "#" + trigger_point_id + " > div";
        $(function() {
                new ScrollMagic.Scene(
                    {
                        triggerElement: trigger
                    }
                )
                .setTween( tween, {y: "80%", ease: Linear.easeNone})
                //.addIndicators()
                .addTo(global_controller_parallax);   
                }
            );     
    }
// * * * * * * * * * * * PINNING BACKGROUND * * * * * * * * * * * //
    Insert_Pinning_Background(section_id, position)
    {
        var section = document.getElementById(section_id);
        var children = section.children;
        var trigger_id;
        var description;
        var components;
        var pinning_element;
        var elements = [];
        for ( var i = 0; i < children.length; i++ )
        {
            var background_image;
            switch(children[i].className)
            {
                case 'section_trigger_point':
                    trigger_id = children[i]
                break;
                case 'section_description':
                    {
                        description = children[i];
                        background_image = children[i].getElementsByClassName("images_sources")[0];
                    }
                break;
                case 'section_pinning_element':
                    pinning_element = children[i]            
                break;
                case 'section_other_description':
                    other_description = children[i];
            }
            for ( var c = 0; c < description.children.length; c++ )
            {
                if (description.children[c].className == 'section_description_components')
                {
                    components = description.children[c];
                }
            }

            description.style.backgroundAttachment = "fixed";
            components.style.paddingTop = "46%";
            components.style.zIndex = "12";
            // this.fixed_backgrounds[section_id] = {
            //     'components' : []
            // }
            elements.push(
                {
                    'id' : components.id,
                    'background' : background_image.src,
                    'start' : components.offsetTop,
                    'height' : components.offsetHeight,
                    'end' : components.offsetHeight
                }
            );
            
            switch( position )
            {
                case 'left':
                    {
                        components.style.marginLeft = "5%";
                        components.style.marginRight = "5%";
                        components.style.textAlign = "left";
                        for (var ch = 0; ch < components.children.length; ch++ )
                        {
                            components.children[ch].style.textAlign = "left";
                        }
                    }
                break;
                case 'center':
                    {
                        
                        var third_width_window = window.innerWidth / 3;
                        components.style.paddingLeft = "" + third_width_window + "px";
                        components.style.paddingRight = "" + third_width_window + "px";
                        components.style.textAlign = "center";
                        for (var ch = 0; ch < components.children.length; ch++ )
                        {
                            components.children[ch].style.textAlign = "center";
                        }
                        
                    }
                break;
                case 'right':
                    {
                        components.style.marginLeft = "5%";
                        components.style.marginRight = "5%";
                        components.style.textAlign = "right";
                        for (var ch = 0; ch < components.children.length; ch++ )
                        {
                            components.children[ch].style.textAlign = "right";
                        }
                    }
                break;
            }
        }
        components.style.paddingBottom = "50%";
        this.fixed_backgrounds[section_id] = {
            'start' : 0,
            'end' : 0,
            'components' : elements
        };
        var scene_duration = description.offsetHeight;
        this.CreateScene(trigger_id, scene_duration, pinning_element);
    }

// * * * * * * * * * * * CREATE SCROLLMAGIC SCENE  * * * * * * * * * * * //
    CreateScene(trigger_id, duration, pin_id)
    {
        
        // in input prendo:
        // trigger_id: punto di trigger, che è un div a se stante che precede il blocco di elementi che subiranno un'alterazione in base allo scroll
        // duration: corrispondente ai pixel di durata del "blocco" dell'elemento (che verrà passato come secondo parametro)
        // pin_id: l'elemento che rimarrà presente nella viewport anche se verrà effettuato uno scrolling

        // NOTA BENE: la duration presa in input in realtà COINCIDE con dei pixel, ma questi sono calcolati AUTOMATICAMENTE dal DOM. Ogni componente DOM ha delle proprie caratteristiche
        // all'interno della viewport. Per risalire alla sua LUNGHEZZA EFFETTIVA, quindi quella che viene OCCUPATA REALMENTE ALLA FINE DEL LOADING DELLA PAGINA WEB, viene riportata
        // nella proprietà _offsetHeight_. Per questo motivo, in input, si prenderà l'offsetHeight del contenuto al quale l'elemento pinnato (quindi l'elemento che deve rimanere presente
        // nella viewport DURANTE l'azione di SCROLLING) deve rimanere ancorato, lasciandocelo per TUTTA LA LUNGHEZZA DEL COMPONENTE
        $(function () {
            // creazione delle varie scene di scrollmagic che verranno poi agganciate al controller globale
            //var scene = 
            
            new ScrollMagic.Scene({
                
                triggerElement: trigger_id,
                triggerHook: 0.1,
                duration: duration
            })
            .setPin(pin_id)
            // .addIndicators({
            //     name: "trigger (duration: " + duration + " px )"
            // })
            .addTo(global_controller);
        });
    }

    // CreateVariousPinnedElementsScene(trigger_id, duration, pin_id)
    // {
        
    //     // in input prendo:
    //     // trigger_id: punto di trigger, che è un div a se stante che precede il blocco di elementi che subiranno un'alterazione in base allo scroll
    //     // duration: corrispondente ai pixel di durata del "blocco" dell'elemento (che verrà passato come secondo parametro)
    //     // pin_id: l'elemento che rimarrà presente nella viewport anche se verrà effettuato uno scrolling

    //     // NOTA BENE: la duration presa in input in realtà COINCIDE con dei pixel, ma questi sono calcolati AUTOMATICAMENTE dal DOM. Ogni componente DOM ha delle proprie caratteristiche
    //     // all'interno della viewport. Per risalire alla sua LUNGHEZZA EFFETTIVA, quindi quella che viene OCCUPATA REALMENTE ALLA FINE DEL LOADING DELLA PAGINA WEB, viene riportata
    //     // nella proprietà _offsetHeight_. Per questo motivo, in input, si prenderà l'offsetHeight del contenuto al quale l'elemento pinnato (quindi l'elemento che deve rimanere presente
    //     // nella viewport DURANTE l'azione di SCROLLING) deve rimanere ancorato, lasciandocelo per TUTTA LA LUNGHEZZA DEL COMPONENTE
    //     $(function () {
    //         // creazione delle varie scene di scrollmagic che verranno poi agganciate al controller globale
    //         //var scene = 
            
    //         new ScrollMagic.Scene({
    //             triggerElement: trigger_id,
    //             triggerHook: 0.1,
    //             duration: duration
    //         })
    //         .setPin(pin_id)
    //         // .addIndicators({
    //         //     name: "trigger (duration: " + duration + " px )"
    //         // })
    //         .addTo(global_controller);
    //     });
    // }

    CreateRevealingScene(trigger_id, pin_id)
    {
        $(function () {
            // creazione delle varie scene di scrollmagic che verranno poi agganciate al controller globale
            new ScrollMagic.Scene({
                triggerElement: trigger_id,
                triggerHook: 0.7, // in genere è 0.5. più è alto, prima parte
                offset: 50,
                reverse: true
            })
            .setClassToggle(pin_id, "visible")
            .addTo(global_controller);
        });
    }

    CreateImagesSequence(trigger_id, duration, tween, pin_id)
    {
        $(function () {
            // creazione delle varie scene di scrollmagic che verranno poi agganciate al controller globale
            new ScrollMagic.Scene({
                triggerElement: trigger_id,
                triggerHook: 0.1,
                duration: duration
            })
            .setPin(pin_id)
            .setTween(tween)
            .addTo(global_controller);
        });
    }

// * * * * * * * * * * * GETs & SETs * * * * * * * * * * * //
    // GETs
    Get_Total_Section()
    {
        return this.total_sections;
    }

    Get_Anchored_Sections()
    {
        return this.anchored_sections;
    }

    Get_Sections_Info()
    {
        return this.sections_info;
    }

    Get_Fixed_Backgrounds()
    {
        return this.fixed_backgrounds;
    }

    Get_Steps_Pinnings_Elements()
    {
        return this.steps_pinnings_elements;
    }

    // SETs
    Set_Total_Sections()
    {
        this.total_sections = [];
        this.sections_info = [];
        this.total_sections = document.getElementsByClassName("sas_section");
    }

    Set_Anchored_Sections()
    {
        this.anchored_sections = [];
        this.sections_info = [];
        this.anchored_sections = document.getElementsByClassName("anchored");
    }

    Set_Sections_Info()
    {
        this.Set_Total_Sections();
        for ( var i = 0; i < this.total_sections.length; i++ )
        {
            var id = this.total_sections[i].id;
            var title = this.total_sections[i].attributes.title.nodeValue;
            this.sections_info.push({
                'id': id, 
                'title': title,
                'start': parseInt(this.total_sections[i].offsetTop),
                'end' : parseInt(this.total_sections[i].offsetTop + this.total_sections[i].getBoundingClientRect().height)
            });
        }
    }

    Set_Fixed_Background()
    {
        var section_id_fixed_backgrounds = Object.keys(this.fixed_backgrounds);
        if ( section_id_fixed_backgrounds.length > 0 )
        {
            var section_top;
            var section_id_fixed_backgrounds = Object.keys( this.fixed_backgrounds );
            for (var k = 0; k < section_id_fixed_backgrounds.length; k++ )
            {
                for ( var i = 0; i < this.sections_info.length; i++ )
                {
                    if ( this.sections_info[i].id == section_id_fixed_backgrounds[k] )
                    {
                        var key = section_id_fixed_backgrounds[k];
                        this.fixed_backgrounds[key].start = this.sections_info[i].start;
                        section_top = this.sections_info[i].start;
                        this.fixed_backgrounds[key].components[0].start = section_top;
                        this.fixed_backgrounds[key].components[0].end =  section_top + this.fixed_backgrounds[key].components[0].height;
                        this.fixed_backgrounds[key].end = this.sections_info[i].end;
                        for ( var j = 1; j < this.fixed_backgrounds[key].components.length; j++ )
                        {
                            var before = j-1;
                            this.fixed_backgrounds[key].components[j].start = this.fixed_backgrounds[key].components[before].end;
                            this.fixed_backgrounds[key].components[j].end = this.fixed_backgrounds[key].components[j].start + this.fixed_backgrounds[key].components[j].height;
                        }
                    }
                }
            }
        }
    }

    Set_Not_Visible_Pinnings_Elements()
    {
        var scroll_a_story_container = document.getElementsByClassName("scroll_a_story_container")[0];
        var sections = scroll_a_story_container.children;
        for ( var i = 0; i < sections.length; i++ )
        {
            var section = sections[i];
            var section_class_list = section.className;
            var section_classes = section_class_list.split(" ");
            for ( var c = 0; c < section_classes.length; c++ )
            {
                var section_class = section_classes[c].split("-");
                var section_type = section_class[0];
                if ( section_type == "variable_pinning_elements" )
                {
                    for ( var j = 0; j < section.children.length; j++ )
                    {
                        if ( section.children[j].className = "section_pinning_element")
                        {
                            var child = section.children[j];
                            for ( var a = 0; a < child.children.length; a++ )
                            {
                                if ( child.children[a].classList == "variable_pinned_element" )
                                {
                                    child.children[a].classList = "not_visible_steps_section_pinnning_element";
                                }
                            }
                            
                        }
                    }
                }
            }
        }
    }

    Set_Steps_Variable_Pinnings_Elements()
    {
        for ( var i = 0; i < this.sections_info.length; i++ )
        {
            var key = this.sections_info[i].id;
            if ( this.steps_pinnings_elements[key] )
            {
                this.steps_pinnings_elements[key].section_start = this.sections_info[i].start; 
                this.steps_pinnings_elements[key].section_end = this.sections_info[i].end; 
                var section_steps = this.steps_pinnings_elements[key];
                var trigg_id = section_steps.section_steps[0].trigg_id;
                var new_offsets_trigg_id = document.getElementById(trigg_id);
                var trigg_end = new_offsets_trigg_id.offsetHeight;
                this.steps_pinnings_elements[key].section_steps[0].triggs_info.start = section_steps.section_start;
                this.steps_pinnings_elements[key].section_steps[0].triggs_info.end = section_steps.section_start + trigg_end;
                for ( var j = 1; j < section_steps.section_steps.length; j++ )
                {
                    var before = j-1;
                    var trigg_id = section_steps.section_steps[j].trigg_id;
                    var new_offsets_trigg_id = document.getElementById(trigg_id);
                    var trigg_offsetTop = new_offsets_trigg_id.offsetTop;
                    this.steps_pinnings_elements[key].section_steps[j].triggs_info.start = this.steps_pinnings_elements[key].section_steps[before].triggs_info.end
                    this.steps_pinnings_elements[key].section_steps[j].triggs_info.end = this.steps_pinnings_elements[key].section_steps[j].triggs_info.start + new_offsets_trigg_id.offsetHeight;
                }
            }
        }
    }

    Set_Steps_Discreete_Pinnings_Elements()
    {
        for ( var i = 0; i < this.sections_info.length; i++ )
        {
            var key = this.sections_info[i].id;
            if ( this.steps_discreete_pinnings_elements[key] )
            {
                this.steps_discreete_pinnings_elements[key].section_start = this.sections_info[i].start; 
                this.steps_discreete_pinnings_elements[key].section_end = this.sections_info[i].end; 
                var section_steps = this.steps_discreete_pinnings_elements[key];
                var triggs = section_steps.triggs;
                var triggs_ids = Object.keys(triggs);
                var first_trigg = document.getElementById(triggs_ids[0]);
                var new_start_first_trigg = first_trigg.offsetTop;
                var new_end_first_trigg = first_trigg.offsetHeight;
                var first_trigg = triggs_ids[0];
                this.steps_discreete_pinnings_elements[key].triggs[first_trigg].start = new_start_first_trigg;
                this.steps_discreete_pinnings_elements[key].triggs[first_trigg].end = new_start_first_trigg + new_end_first_trigg;
                for ( var tr = 1; tr < triggs_ids.length; tr ++) 
                {
                    var before = tr - 1;
                    var pre_trigg_id = triggs_ids[before];
                    var trigg_id = triggs_ids[tr];
                    var new_offsets_trigg_id = document.getElementById(trigg_id);
                    var trigg_offsetHeight = new_offsets_trigg_id.offsetHeight; 
                    this.steps_discreete_pinnings_elements[key].triggs[trigg_id].start = this.steps_discreete_pinnings_elements[key].triggs[pre_trigg_id].end;
                    this.steps_discreete_pinnings_elements[key].triggs[trigg_id].end = this.steps_discreete_pinnings_elements[key].triggs[trigg_id].start + trigg_offsetHeight;
                }
            }
        }
    }

// * * * * * * * * * * * ADDITIONAL FUNCTIONS * * * * * * * * * * * //
    Add_PlaceHolder_Anchor(inAnchoredSections, inPosition)
    {
        // inserimento dei segnaposto 
        var placeholder_div = document.getElementById("anchor_points");
        var placeholder_div_style = document.createAttribute("style");
        placeholder_div_style.value = "position: fixed; z-index: 11;";
        placeholder_div.setAttributeNode(placeholder_div_style);
        var div_placeholder_style = document.getElementById("anchor_points").getAttribute("style");
        var set_position;

        if ( inPosition == "right"){
            set_position = div_placeholder_style += " margin-left: 99%;";
        }
        else
            set_position = div_placeholder_style += " margin-left: 2.5em";

        var set_style_anchor_points = document.createAttribute("style");
        set_style_anchor_points.value = set_position;
        document.getElementById("anchor_points").setAttributeNode(set_style_anchor_points);
        var ul = document.createElement("ul");
        var id_ul = document.createAttribute("id");
        id_ul.value = "placeholder_entry";
        ul.setAttributeNode(id_ul);
        for ( var i = 0; i < inAnchoredSections.length; i++ )
        {
            var li = document.createElement("li");
            var span =  document.createElement("span");
            span.innerHTML = inAnchoredSections[i].title;
            var span_id = document.createAttribute("id");
            span_id.value = "li_" + inAnchoredSections[i].id;
            var span_class = document.createAttribute("class");
            span_class.value = "not_visible_placeholder_li";
            span.setAttributeNode(span_id);
            span.setAttributeNode(span_class);
            li.appendChild(span);
            var a = document.createElement("a");
            var a_class_name = document.createAttribute("class");
            var a_id = document.createAttribute("id");

            var a_hovering = document.createAttribute("onmouseover");
            a_hovering.value = "var li = document.getElementById('li_" + inAnchoredSections[i].id + "'); li.className='visible_placeholder_li';";
            var a_mouseout = document.createAttribute("onmouseout");
            a_mouseout.value = "var li = document.getElementById('li_" + inAnchoredSections[i].id + "'); li.className='not_visible_placeholder_li';";
            a.setAttributeNode(a_hovering);
            a.setAttributeNode(a_mouseout);

            a_class_name.value = "visible_anchor_point";
            a_id.value = inAnchoredSections[i].id + "_placeholder";
            a.setAttributeNode(a_class_name);
            a.setAttributeNode(a_id);
            var ref = inAnchoredSections[i].id;
            var href = document.createAttribute("href");
            href.value="#"+ref;

            var ul_style = document.createAttribute("style");
            var li_style = document.createAttribute("style");
            var span_style = document.createAttribute("style");
            
            if ( inPosition == "right" ){
                var margin_left_span = (inAnchoredSections[i].id.length) ;
                ul_style.value = "text-align: right;";
                li_style.value = "text-align: right;";
                span_style.value = "margin-left: -" + margin_left_span + "em; padding-right: 1.5em;"
                span.setAttributeNode(span_style);
                li.setAttributeNode(li_style);
                ul.setAttributeNode(ul_style);
            }          

            a.setAttributeNode(href);
            a.appendChild(li);
            ul.appendChild(a);
        }        
        document.getElementById("anchor_points").appendChild(ul);
        var anchor_points = document.getElementById("anchor_points");
        var anchor_points_BoundingClientRect = anchor_points.getBoundingClientRect();
        if ( inPosition === 'left' ){
            var sas_sections = document.getElementsByClassName("sas_section");
            for ( var i = 0; i < sas_sections.length; i++ ){
                var sas_section = sas_sections[i];
                var style = document.createAttribute("style");
                style.value = 'margin-left: ' + (anchor_points_BoundingClientRect.x / 2) +"px";
                //sas_section.setAttributeNode(style);
            }
        }
        console.log("window.innerWidth ", window.innerWidth);
        console.log(" (anchor_points_BoundingClientRect.x / 2) ",  (anchor_points_BoundingClientRect.x / 2))
    }    

    // * * * * * * * * * * * SCROLL's FUNCTIONS * * * * * * * * * * * //
    Dynamic_Placeholders()
    {
        // creazione dei segnaposto dinamici
        // vengono visualizzati i segnaposto in base alla posizione dell'utente nella pagina
        this.Set_Total_Sections();
        this.Set_Sections_Info();
        if ( window.pageYOffset + window.innerHeight >= this.sections_info[this.sections_info.length-1].end )
        {
            for( var i = 0; i < this.sections_info.length; i++ )
            {
                var a_placeholder_entry = document.getElementById(this.sections_info[i].id+"_placeholder");
                if ( a_placeholder_entry != null )
                {
                    a_placeholder_entry.classList.value = "visible_anchor_point";
                }
            }
        }
        for( var i = 0; i < this.sections_info.length; i++ )
        {
            if ( window.pageYOffset > this.sections_info[i].end ){
                var a_placeholder_entry = document.getElementById(this.sections_info[i].id+"_placeholder");
                if ( a_placeholder_entry != null )
                {
                    a_placeholder_entry.classList.value = "visible_anchor_point";
                }
                
            }
        }
    }

    Change_Background_After_Scolling()
    {
        // cambio dello sfondo in base alla posizione dell'utente sulla pagina
        // utilizzata SOLO se è presente una sezione con classe 'text_on_fixed_backgrounds'
        var section_id_fixed_backgrounds = Object.keys(this.fixed_backgrounds);
        if ( section_id_fixed_backgrounds.length > 0 )
        {
            this.Set_Fixed_Background();
            var text_on_fixed_backgrounds = document.getElementsByClassName("photography_section");
            for ( var t = 0; t < text_on_fixed_backgrounds.length; t++ )
            {
                var t_id = text_on_fixed_backgrounds[t].id;
                if (window.pageYOffset < this.fixed_backgrounds[t_id].components[0].start)
                {
                    text_on_fixed_backgrounds[t].style.backgroundImage = "url('" + this.fixed_backgrounds[t_id].components[0].background + "')";
                } 
                else if ( window.pageYOffset > this.fixed_backgrounds[t_id].components[this.fixed_backgrounds[t_id].components.length-1].start) 
                {
                    text_on_fixed_backgrounds[t].style.backgroundImage = "url('" + this.fixed_backgrounds[t_id].components[this.fixed_backgrounds[t_id].components.length-1].background + "')";
                } 
                else 
                {
                    for ( var i = 0; i < this.fixed_backgrounds[t_id].components.length; i++ )
                    {
                        if ( (window.pageYOffset > this.fixed_backgrounds[t_id].components[i].start) && (window.pageYOffset < this.fixed_backgrounds[t_id].components[i].end) )
                        {
                            text_on_fixed_backgrounds[t].style.backgroundImage = "url('" + this.fixed_backgrounds[t_id].components[i].background + "')";
                        }
                    }
                }
            }
        }
    }

    Change_Pinning_Element()
    {
        // in base alla posizione dell'utente nella pagina, si aggiorna l'elemento
        // pinnato nella sezione
        this.Set_Sections_Info();
        this.Set_Steps_Variable_Pinnings_Elements();
        for( var i = 0; i < this.sections_info.length; i++ )
        {
            var key = this.sections_info[i].id
            if ( this.steps_pinnings_elements[key] )
            {
                var section = this.steps_pinnings_elements[key];
                if ( (window.pageYOffset + (window.innerHeight / 2) > section.section_start) && window.pageYOffset < section.section_end )
                {
                    var step =  document.getElementById(section.section_steps[0].pin_id);
                    step.classList.value = "visible_steps_section_pinnning_element";

                    for ( var j = 0; j < section.section_steps.length; j++ )
                    {
                        if ( (window.pageYOffset + (window.innerHeight / 2) > section.section_steps[j].triggs_info.start) && (window.pageYOffset + (window.innerHeight / 2) < section.section_steps[j].triggs_info.end) )
                        {
                            var step =  document.getElementById(section.section_steps[j].pin_id);
                            step.classList.value = "visible_steps_section_pinnning_element";
                        } else {
                            var step =  document.getElementById(section.section_steps[j].pin_id);
                            step.classList.value = "not_visible_steps_section_pinnning_element";
                        }

                        if ( (window.pageYOffset + (window.innerHeight / 2) < section.section_steps[0].triggs_info.start) )
                        {
                            var step =  document.getElementById(section.section_steps[0].pin_id);
                            step.classList.value = "visible_steps_section_pinnning_element";
                        }

                        if ( j ==  section.section_steps.length-1 )
                        {
                            if ( (window.pageYOffset + (window.innerHeight / 2) > section.section_steps[j].triggs_info.end) )
                            { 
                                var step =  document.getElementById(section.section_steps[j].pin_id);
                                step.classList.value = "visible_steps_section_pinnning_element";
                            }
                        }
                    }
                }
            }
        }
    }

    Use_Discreete_Script()
    {
        var keys_steps_advanced = Object.keys(SAS.steps_discreete_pinnings_elements);
        for ( var k = 0; k < keys_steps_advanced.length; k++ )
        {
            var key = keys_steps_advanced[k];

            if ( window.pageYOffset > SAS.steps_discreete_pinnings_elements[key].section_start )
            {
                this.Set_Steps_Discreete_Pinnings_Elements();
                var section = document.getElementById(key);
                var trigs_keys = Object.keys(SAS.steps_discreete_pinnings_elements[key].triggs);
                for ( var t = 0; t < trigs_keys.length; t++)
                {
                    var t_key = trigs_keys[t];
                    var trigging = SAS.steps_discreete_pinnings_elements[key].triggs[t_key];
                    if ( (window.pageYOffset + window.innerHeight / 2) > trigging.start && (window.pageYOffset + window.innerHeight / 2) < trigging.end )
                    {
                        var script = eval(section.attributes.onsasscroll.value)(trigging.value);
                        if ( script )
                            script();
                    }
                }
            }                                
        }
    }

    Use_Continuous_Script()
    {
        var keys_steps_advanced = Object.keys(SAS.steps_continuous_pinnings_elements);
        for ( var k = 0; k < keys_steps_advanced.length; k++ )
        {
            var key = keys_steps_advanced[k];
            if ( window.pageYOffset > SAS.steps_continuous_pinnings_elements[key].section_start )
            {
                if (  (window.pageYOffset  > SAS.steps_continuous_pinnings_elements[key].section_start) && (window.pageYOffset < SAS.steps_continuous_pinnings_elements[key].section_end) )
                {
                    var section = document.getElementById(key);
                    var height = SAS.steps_continuous_pinnings_elements[key].section_height;
                    var start = SAS.steps_continuous_pinnings_elements[key].section_start;
                    var value = ( ( window.pageYOffset - start) * 100 ) / height;
                    if ( value > 0 && value < 100 )
                    {
                        var script = eval(section.attributes.onsasscroll.value)(value);
                        if ( script )
                        {
                            script();
                        }
                    }
                }
                else
                {
                    return;
                }       
            }
            else
            {
                return;
            }
        }
    }

// * * * * * * * * * * * PAGE CREATION * * * * * * * * * * * //
    Create_Scroll_A_Story_Page()
    {
        var scroll_a_story_container = document.getElementsByClassName("scroll_a_story_container")[0];
        var container_classes = scroll_a_story_container.className.split(" ");
        var container_parameters = [];
        var position_anchor_points;
        if ( container_classes.length == 1 &&  container_classes[0] == "scroll_a_story_container")
        {
            this.show_anchor_points = false;
            this.type_anchor_points = null;
        } 
        else
        {
            for (var cc = 0; cc < container_classes.length; cc++ )
            {
                if ( container_classes[cc] != "scroll_a_story_container" )
                    {
                        container_parameters.push(container_classes[cc]);
                    }
            }
            var parameters = container_parameters[0].split("-");
            for ( var param = 0; param < parameters.length; param++ )
            {
                var parameter = parameters[param];

                switch(parameter)
                {
                    case 'left':
                    {
                        this.show_anchor_points = true;
                        position_anchor_points = "left";
                    }
                    break;
                    case 'right':
                    {
                        this.show_anchor_points = true;
                        position_anchor_points = "right";
                    }
                    break;
                    case 'dynamic':
                    {
                        this.type_anchor_points = "dynamic";
                        this.show_anchor_points = true;
                    }
                    break;
                    case 'static':
                    {
                        this.type_anchor_points = "static";
                        this.show_anchor_points = true;
                    }
                    break;
                    
                }
            }
        }
        var sections = scroll_a_story_container.children;
        for ( var i = 0; i < sections.length; i++ )
        {
            var section = sections[i];
            var section_id = sections[i].id;
            var section_class_list = section.className;
            var section_classes = section_class_list.split(" ");
            for ( var c = 0; c < section_classes.length; c++ )
            {
                var section_class = section_classes[c].split("-");
                var section_type;
                var first_parameter;
                var second_parameter;
                if ( section_class.length > 1 )
                {
                    for ( var p = 0; p < section_class.length; p++ )
                    {
                        var class_parameters = section_class[p].split(".");
                        if ( class_parameters.length > 0 )
                        {
                            section_type = section_class[0];
                            first_parameter = class_parameters[0];
                            second_parameter = class_parameters[1]
                        }
                        else 
                        {
                            section_type = section_class[0];
                            first_parameter = section_class[1];
                        }
                    }
                }
                else
                {
                    section_type = section_class[0];
                    first_parameter = section_class[1];
                }

                if ( first_parameter == undefined )
                {
                    first_parameter = "left";
                }
                if ( second_parameter == undefined )
                {
                    second_parameter = "50";
                }

                switch (section_type)
                {
                    
                    case 'pinning_element_section':
                        {
                            this.Insert_Pinning_Element(section_id, first_parameter, second_parameter);
                        }
                    break;
                    case 'image_sequence_section':
                        {
                            var images_sources = sections[i].getElementsByClassName("images_sources");
                            var sources = []
                            for ( var img = 0; img < images_sources.length; img++ )
                            {
                                sources.push(images_sources[img].src);
                            }
                            this.Insert_Pinning_Image_Sequence(section_id, sources, first_parameter, second_parameter);
                        }
                    break;  
                    case 'revealing_element_section':
                        {
                            this.Insert_Revealing_Component(section_id, first_parameter);
                        }
                    break;
                    case 'text_on_fixed_backgrounds':
                        {
                            this.Insert_Pinning_Background(section_id, first_parameter);
                            
                            this.Set_Fixed_Background();
                        }
                    break;
                    case 'variable_pinning_elements':
                        {
                            this.Insert_Variable_Pinnings_Elements(section_id, first_parameter, second_parameter);
                        }
                    break;
                    case 'advanced_pinning_element':
                        {
                            this.Set_Parameters_For_User_Script(section_id, first_parameter, second_parameter);
                        }
                    break;
                    case 'discreete_pinning_element':
                        {
                            this.Insert_Discreete_Pinning_Element(section_id, first_parameter, second_parameter);
                        }
                    break;
                    case 'continuous_pinning_element':
                        {
                            this.Insert_Continuous_Pinning_Element(section_id, first_parameter, second_parameter);
                        }
                    break;
                }
            }
        }        
        this.Set_Total_Sections();
        this.Set_Sections_Info();
        if ( !isEmpty(this.fixed_backgrounds) )
        {
            this.Set_Fixed_Background();
        }
        if ( this.steps_pinnings_elements.length > 0)
        {
            this.Set_Steps_Variable_Pinnings_Elements();
        }
        if ( this.show_anchor_points )
        {
            this.Set_Anchored_Sections();
            this.Add_PlaceHolder_Anchor(SAS.Get_Anchored_Sections(), position_anchor_points);
            if ( this.type_anchor_points == "dynamic")
            {
                var placeholder_entry = document.getElementById("placeholder_entry");
                var a_placeholder_entry = placeholder_entry.getElementsByTagName("a");
                for ( var i = 0; i < a_placeholder_entry.length; i++ )
                {
                    a_placeholder_entry[i].classList.value = "not_visible_anchor_point";
                }
            }
        }
        else 
        {            
            if ( this.fixed_backgrounds.length > 0 )
            {
                this.Set_Fixed_Background();
            }
            if ( this.steps_pinnings_elements.length > 0)
            {
                this.Set_Steps_Variable_Pinnings_Elements();
            }
        }
    }

    Init()
    {
        window.addEventListener('load', function(){ 
            SAS.Create_Scroll_A_Story_Page();
            SAS.Set_Not_Visible_Pinnings_Elements();
            SAS.Set_Total_Sections();
            SAS.Set_Sections_Info();
            if ( SAS.fixed_backgrounds != undefined )
            {
                SAS.Change_Background_After_Scolling();
            }
            SAS.is_ready = true;
        });
        
        document.addEventListener("scroll", function(){ 
            
            SAS.Set_Total_Sections();
            SAS.Set_Sections_Info();

            if ( SAS.steps_pinnings_elements != undefined )
            {
                SAS.Change_Pinning_Element();
            }
        
            if ( SAS.fixed_backgrounds != undefined )
            {
                SAS.Change_Background_After_Scolling();
            }

            if (SAS.type_anchor_points == "dynamic") 
            {
                SAS.Dynamic_Placeholders();
            } 

            if ( SAS.steps_discreete_pinnings_elements )
            {
                SAS.Use_Discreete_Script();
            }

            if ( SAS.steps_continuous_pinnings_elements )
            {
                SAS.Use_Continuous_Script();
            }
            
            
        });
        
        window.addEventListener('resize', function() { 
            location.reload(); 
        });

    }
}

function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}