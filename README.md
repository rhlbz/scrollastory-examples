# ScrollAStory.js

(aprile 2019 - settembre 2019)

ScrollAStory.js è una libreria che facilita l'implementazione e la composizione di pagine web che rispondono al paragigma di visualizzazione dello scrollytelling.
La libreria class-based si basa sull'uso di classi css strutturate secondo un sistema gerarchico ben definito che permette alla libreria di farsi carico della effettiva renderizzazione delle diverse tipologie di effetti grafici tipici del paradigma in oggetto.

_La libreria in oggetto costiuisce parte integrante del progetto di tesi presentato alla commissione di laurea che il 3 ottobre 2019 mi ha proclamata dottoressa magistrale in Informatica Umanistica con votazione 110L_
