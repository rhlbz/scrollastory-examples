var SAS = new ScrollAStory();

window.addEventListener('load', function(){ 
    // PARAMETRI NECESSARI:
    // visualizzazione dei anchoreds: true || false 
    // posizionamento dei anchoreds: left || right 
    // tipologia di anchoreds: static || dynamic
    SAS.Create_Tell_A_Scrolly_Page(true, "left", "static");
    SAS.Set_Not_Visible_Pinnings_Elements();

    var parameters_addictional1 = SAS.Get_Parameters_For_User_Script("addictional_1");
    var parameters_addictional2 = SAS.Get_Parameters_For_User_Script("addictional_2");
    Pinned_Line_Graph(parameters_addictional1.trigger_id, parameters_addictional1.scene_duration, parameters_addictional1.controller);
    Pinned_Line_Graph_2(parameters_addictional2.trigger_id, parameters_addictional2.scene_duration, parameters_addictional2.controller);
});

document.addEventListener("scroll", function(){ 
    if ( SAS.steps_pinnings_elements != undefined )
    {
        SAS.Set_Total_Sections();
        SAS.Set_Sections_Info();
        SAS.Change_Pinning_Element();
    }

    if ( SAS.fixed_backgrounds.length > 0 )
    {
        SAS.Change_Background_After_Scolling();
    }
    
    if (SAS.type_anchoreds == "dynamic") 
    {
        SAS.Dynamic_Placeholders();
    } 
    else
    {
        SAS.Set_Total_Sections();
        SAS.Set_Sections_Info();
    }
});

window.addEventListener('resize', function() { 
    location.reload(); 
});
