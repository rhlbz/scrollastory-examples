var SAS = new ScrollAStory();

window.addEventListener('load', function(){ 
    // PARAMETRI NECESSARI:
    // visualizzazione dei anchoreds: true || false 
    // posizionamento dei anchoreds: left || right 
    // tipologia di anchoreds: static || dynamic
    SAS.Create_Tell_A_Scrolly_Page(true, "left", "static");
    SAS.Set_Not_Visible_Pinnings_Elements();
});

document.addEventListener("scroll", function(){ 
    if ( SAS.steps_pinnings_elements != undefined )
    {
        SAS.Set_Total_Sections();
        SAS.Set_Sections_Info();
        SAS.Change_Pinning_Element();
    }

    if ( SAS.fixed_backgrounds.components.length > 0 )
    {
        SAS.Change_Background_After_Scolling();
    }
    
    if (SAS.type_anchoreds == "dynamic") 
    {
        SAS.Dynamic_Placeholders();
    } 
    else
    {
        SAS.Set_Total_Sections();
        SAS.Set_Sections_Info();
    }
});

window.addEventListener('resize', function() { 
    location.reload(); 
});
