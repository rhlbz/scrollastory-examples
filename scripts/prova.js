function Pinned_Line_Graph(trigger_id, scene_duration, controller)
{
    console.log("  DENTRO PROVA.JS  ");
    console.log("DENTRO A PROVA.JS");
    function pathPrepare ($el) {
      //console.log($el);
      var lineLength = $el[0].getTotalLength();
      $el.css("stroke-dasharray", lineLength);
      $el.css("stroke-dashoffset", lineLength);
    }
    console.log(trigger_id);
    var $word = $("path.line");
    
    // prepare SVG
    //console.log($word);
    pathPrepare($word);
    
    // init controller
    //console.log(scene_duration);
    // build tween
    var tween = new TimelineMax()
      .add(TweenMax.to($word, 0.9, {strokeDashoffset: 0, ease:Linear.easeNone})) // draw word for 0.9
      //.add(TweenMax.to("path", 1, {stroke: "#33629c", ease:Linear.easeNone}), 0);			// change color during the whole thing
    
    
    // build scene
    var scene = new ScrollMagic.Scene({triggerElement: trigger_id, duration: scene_duration, tweenChanges: true})
            .setTween(tween)
            .addIndicators() // add indicators (requires plugin)
            .addTo(controller);

    var $word = null;
    
}

function Pinned_Line_Graph_2(trigger_id, scene_duration, controller)
{
    console.log("  DENTRO PROVA.JS  ");
    console.log("DENTRO A PROVA.JS");
    function pathPrepare ($el) {
      //console.log($el);
      var lineLength = $el[0].getTotalLength();
      $el.css("stroke-dasharray", lineLength);
      $el.css("stroke-dashoffset", lineLength);
    }
    
    var $word = $("path.line_2");
    
    // prepare SVG
    //console.log($word);
    pathPrepare($word);
    
    // init controller
    //console.log(scene_duration);
    // build tween
    var tween = new TimelineMax()
      .add(TweenMax.to($word, 0.9, {strokeDashoffset: 0, ease:Linear.easeNone})) // draw word for 0.9
      //.add(TweenMax.to("path", 1, {stroke: "#33629c", ease:Linear.easeNone}), 0);			// change color during the whole thing
    
    
    // build scene
    var scene = new ScrollMagic.Scene({triggerElement: trigger_id, duration: scene_duration, tweenChanges: true})
            .setTween(tween)
            .addIndicators() // add indicators (requires plugin)
            .addTo(controller);

    var $word = null;
    
}